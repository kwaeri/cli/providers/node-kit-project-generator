/**
 * SPDX-PackageName: kwaeri/node-kit-project-generator
 * SPDX-PackageVersion: 0.9.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'

// INCLUDES
import * as assert from 'assert';
import * as _fs from 'node:fs/promises';
import { NodeKitProjectGenerator } from '../src/project-generator.mjs';
import { Filesystem } from '@kwaeri/filesystem';
import { Progress } from '@kwaeri/progress';
import debug from 'debug';


// DEFINES
let GENERATOR_ENV = process.env.NODE_ENV || "default";

const progress  = new Progress({ spinner: true, spinAnim: "dots", percentage: false }),
        projectGenerator  = new NodeKitProjectGenerator(
            GENERATOR_ENV !== "test" ? progress.getHandler() : undefined,
                        { environment: GENERATOR_ENV  } as any
                    );

const DEBUG = debug( 'kue:node-kit-project-generator-test' );

// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {

        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    () => {
                        assert.equal( [1,2,3,4].indexOf(4), 3 );
                    }
                );

            }
        );

    }
);


describe(
    'NodeKitProjectGenerator Functionality Test Suite',
    () => {

        describe(
            'Get Service Type Test',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        return Promise.resolve(
                            assert.equal( projectGenerator.serviceType, "Generator Service" )
                        );
                    }
                );
            }
        );

        describe(
            'Get Service Provider Subscriptions Test',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify( projectGenerator.getServiceProviderSubscriptions() ),
                                JSON.stringify( {
                                            "commands": { "new": { "project": true } },
                                            "required": { "new": { "project": { "type": ["api", "react"] } } },
                                            "optional": { "new": { "project": { "redux": { "for": "type=react", "flag": true }, "lang": { "for": false,"flag": false, "values": ["typescript", "javascript"] }, "skip-wizard": { "for": false, "flag": true } } } } } ) )
                        );
                    }
                );
            }
        );

        describe(
            'Get Service Provider Subscriptions Help Text Test',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify( projectGenerator.getServiceProviderSubscriptionHelpText() ),
                                        JSON.stringify(
                                    {
                                        helpText: {
                                            "commands": {   // List Help Text for our commands:
                                                "new": {    // ⇦ For new
                                                    "description": "The 'new' command automates content creation.",
                                                    "specifications": { // ⇦ For the specifications
                                                        "project": {    // ⇦ For project
                                                            "description": "Creates a new empty project of the type specified, and according to options provided.",
                                                            "options": {        // ⇦ For the options of
                                                                "required": {   // ⇦ Required options are specific to the specification, 'project' in this case
                                                                    "type": {   // ⇦ type is a required option
                                                                        "description": "Denotes the type of the project that will be generated.",
                                                                        "values": {
                                                                            "api": {
                                                                                "desccription": "A NodeKit based MV(A)C API project."
                                                                            },
                                                                            "react": {
                                                                                "description": "A NodeKit based client-side React project",
                                                                            }
                                                                        }
                                                                    }
                                                                },
                                                                "optional": {   // ⇦ Optional options can be for the specification, or for the required options
                                                                    "specification": {  // ⇦ For the specification
                                                                        "language": {   // ⇦ List options
                                                                            "description": "Denotes the programming language for the project being generated.",
                                                                            "values": [
                                                                                "typescript",
                                                                                "javascript"
                                                                            ]
                                                                        }
                                                                    },  // ⇦ The various required options that allow optional flags
                                                                    "type": {   // ⇦ For the "type" required option
                                                                        "react": {  // ⇦ For the required options value, can be 'any'
                                                                            "redux": {  // ⇦ List options
                                                                                "description": "Denotes that the project should include redux support",
                                                                                "values": false
                                                                            }
                                                                        }
                                                                    }

                                                                }
                                                            }
                                                        }
                                                    },
                                                    "options": {    // ⇦ For the options of a command (where there isn't specifications)
                                                        "optional": { }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                )
                            )
                        );
                    }
                );
            }
        );

        describe(
            'Project Generator Service Provider Render Service Test [I "REST" Type, From Remote]',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        if( GENERATOR_ENV !== "test" )
                            progress.init();

                        let testMethodResult;
                        /* The configuration passed in mocks that of a KwaeriConfiguration, which includes ProjectBits -
                           Don't let it scare you :D
                        */
                       try {
                            testMethodResult = await projectGenerator.renderService( { quest: "new", specification: "project", subCommands: ["Test Project"], args: { type: "rest"}, version: "1.2.3", configuration: { project: { name: "", type: "", tech: "", repository: "", author: { fullName: "", first: "", last: "", email: "" }, license: { identifier: "" }, copyright: "", copyrightEmail: "", root: "" } } } );
                            if( testMethodResult && ( testMethodResult as any ).result )
                                await _fs.rm( "test-project", { recursive: true, force: true } );

                       } catch( error ) {
                           DEBUG( error );
                       }

                        return Promise.resolve(
                            assert.equal( JSON.stringify( testMethodResult ), JSON.stringify( { type: "create_project_from_remote", result: true } ) )
                        );
                    }
                );
            }
        );

        describe(
            'Project Generator Service Provider Render Service Test [II "REST" Type, From Cache]',
            () => {
                it(
                    'Should return true.',
                    async () => {
                        if( GENERATOR_ENV !== "test" )
                            progress.init();

                        let testMethodResult;
                        /* The configuration passed in mocks that of a KwaeriConfiguration, which includes ProjectBits -
                           Don't let it scare you :D
                        */
                       try {
                            testMethodResult = await projectGenerator.renderService( { quest: "new", specification: "project", subCommands: ["Test Project"], args: { type: "rest"}, version: "1.2.3", configuration: { project: { name: "", type: "", tech: "", repository: "", author: { fullName: "", first: "", last: "", email: "" }, license: { identifier: "" }, copyright: "", copyrightEmail: "", root: "" } } } );
                            if( testMethodResult && ( testMethodResult as any ).result ) {
                                await _fs.rm( "test-project", { recursive: true, force: true } );
                                await _fs.rm( ".kue_cache", { recursive: true, force: true } );
                            }
                       } catch( error ) {
                           DEBUG( error );
                       }

                        return Promise.resolve(
                            assert.equal( JSON.stringify( testMethodResult ), JSON.stringify( { type: "create_project_from_cache", result: true } ) )
                        );
                    }
                );
            }
        );

        describe(
            'Project Generator Service Provider Render Service Test [III "fake" Type]',
            () => {
                it(
                    'Should fail abruptly with an appropriate error.',
                    async () => {
                        let testMethodResult
                        try {
                            /* The configuration passed in mocks that of a KwaeriConfiguration, which includes ProjectBits -
                               Don't let it scare you :D
                            */
                            testMethodResult = await projectGenerator.renderService( { quest: "new", specification: "project", subCommands: ["Test Project"], args: { type: "fake"}, version: "1.2.3", configuration: { project: { name: "", type: "", tech: "", repository: "", author: { fullName: "", first: "", last: "", email: "" }, license: { identifier: ""  }, copyright: "", copyrightEmail: "", root: "" } } } );

                        } catch( exception ) {

                            return Promise.resolve(
                                assert.equal( ( exception as Error ).message, "[ASSEMBLE_OPTIONS] Provided project type 'fake' not supported." )
                            );
                        }
                    }
                );



            }
        );
    }
);

